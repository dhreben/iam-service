FROM adoptopenjdk:11-jre-hotspot
COPY build/libs/iam-service.jar /app/iam-service.jar
CMD ["java", "-jar", "/app/iam-service.jar"]
EXPOSE 8080
