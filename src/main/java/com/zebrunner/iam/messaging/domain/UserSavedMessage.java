package com.zebrunner.iam.messaging.domain;

import com.zebrunner.iam.domain.entity.Source;
import com.zebrunner.iam.domain.entity.UserStatus;
import lombok.Builder;
import lombok.Value;

import java.util.Set;

@Value
@Builder
public class UserSavedMessage {

    Integer id;
    String username;
    String email;
    String firstName;
    String lastName;
    String photoUrl;
    Source source;
    UserStatus status;
    Set<Integer> groupIds;
    Set<String> permissions;

}
