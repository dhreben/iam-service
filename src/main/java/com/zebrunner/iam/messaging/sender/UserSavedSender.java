package com.zebrunner.iam.messaging.sender;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Permission;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.messaging.MessageHelper;
import com.zebrunner.iam.messaging.config.RabbitConfig;
import com.zebrunner.iam.messaging.domain.UserSavedMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserSavedSender {

    private final MessageHelper messageHelper;

    public void sendNotification(User user) {
        UserSavedMessage message = UserSavedMessage.builder()
                                                   .id(user.getId())
                                                   .username(user.getUsername())
                                                   .email(user.getEmail())
                                                   .firstName(user.getFirstName())
                                                   .lastName(user.getLastName())
                                                   .photoUrl(user.getPhotoUrl())
                                                   .source(user.getSource())
                                                   .status(user.getStatus())
                                                   .groupIds(getGroupIds(user))
                                                   .permissions(getPermissionNames(user))
                                                   .build();
        messageHelper.send(RabbitConfig.USER_SAVED_EXCHANGE, message);
    }

    private Set<Integer> getGroupIds(User user) {
        return user.getGroups()
                   .stream()
                   .map(Group::getId)
                   .collect(Collectors.toSet());
    }

    private Set<String> getPermissionNames(User user) {
        return user.getPermissions()
                   .stream()
                   .map(Permission::getName)
                   .collect(Collectors.toSet());
    }

}
