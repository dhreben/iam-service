package com.zebrunner.iam.messaging.listener;

import com.zebrunner.iam.domain.entity.LdapConfiguration;
import com.zebrunner.iam.messaging.config.RabbitConfig;
import com.zebrunner.iam.messaging.domain.IntegrationSavedMessage;
import com.zebrunner.iam.service.LdapService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class IntegrationSaveListener {

    private final LdapService ldapService;
    private final Map<String, Consumer<IntegrationSavedMessage>> integrationTypeToHandler = Map.of(
            "LDAP", this::handleLdapIntegrationSave
    );

    @RabbitListener(queues = RabbitConfig.INTEGRATION_SAVED_IAM_SERVICE_QUEUE)
    public void handleIntegrationSavedMessage(IntegrationSavedMessage eventMessage) {
        Consumer<IntegrationSavedMessage> handler = integrationTypeToHandler.get(eventMessage.getName());
        if (handler != null) {
            handler.accept(eventMessage);
        }
    }

    private void handleLdapIntegrationSave(IntegrationSavedMessage eventMessage) {
        LdapConfiguration ldapConfiguration = buildLdapConfiguration(eventMessage);
        ldapService.saveConfiguration(ldapConfiguration);
    }

    private LdapConfiguration buildLdapConfiguration(IntegrationSavedMessage eventMessage) {
        Map<String, String> paramNameToValue = eventMessage.getParams()
                                                           .stream()
                                                           .collect(Collectors.toMap(
                                                                   IntegrationSavedMessage.Param::getName,
                                                                   IntegrationSavedMessage.Param::getValue
                                                           ));
        String passwordKey = eventMessage.getParams()
                                         .stream()
                                         .filter(param -> "LDAP_MANAGER_PASSWORD".equals(param.getName()))
                                         .findFirst()
                                         .map(IntegrationSavedMessage.Param::getKey)
                                         .orElse(null);
        return LdapConfiguration.builder()
                                .enabled(eventMessage.isEnabled())
                                .url(paramNameToValue.get("LDAP_URL"))
                                .managerUser(paramNameToValue.get("LDAP_MANAGER_USER"))
                                .managerPassword(paramNameToValue.get("LDAP_MANAGER_PASSWORD"))
                                .dn(paramNameToValue.get("LDAP_DN"))
                                .searchFilter(paramNameToValue.get("LDAP_SEARCH_FILTER"))
                                .passwordKey(passwordKey)
                                .build();
    }

}
