package com.zebrunner.iam.domain.search;

import com.zebrunner.iam.domain.entity.UserStatus;
import lombok.Getter;

@Getter
public class UserSearchCriteria extends SearchCriteria {

    UserStatus status;

    public UserSearchCriteria(String query,
                              String sortBy,
                              SortOrder sortOrder,
                              Integer page,
                              Integer pageSize,
                              boolean isPublic,
                              UserStatus status) {
        super(query, sortBy, sortOrder, page, pageSize, isPublic);
        this.status = status;
    }
}
