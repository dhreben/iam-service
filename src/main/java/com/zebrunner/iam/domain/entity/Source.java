package com.zebrunner.iam.domain.entity;

import java.util.Arrays;

public enum Source {

    INTERNAL,
    LDAP;

    public static Source fromString(String value) {
        return Arrays.stream(Source.values())
                     .filter(status -> status.name().equals(value))
                     .findFirst()
                     .orElse(null);
    }

}
