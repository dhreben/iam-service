package com.zebrunner.iam.domain.token;

import lombok.Value;

import java.time.Instant;

@Value
public class RefreshTokenContent {

    Integer userId;
    String password;
    Instant expiration;

}
