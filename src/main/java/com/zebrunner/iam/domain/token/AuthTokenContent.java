package com.zebrunner.iam.domain.token;

import lombok.Value;

import java.util.Set;

@Value
public class AuthTokenContent {

    Integer userId;
    String username;
    Set<String> permissions;

}
