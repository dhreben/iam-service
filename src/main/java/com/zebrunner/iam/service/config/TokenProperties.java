package com.zebrunner.iam.service.config;

import com.zebrunner.iam.service.exception.ServiceInitializationException;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

@Data
@ConfigurationProperties(prefix = "service.token")
public class TokenProperties {

    private Auth auth;
    private PasswordReset passwordReset;
    private Invitation invitation;

    @PostConstruct
    private void init() {
        if (auth == null
                || StringUtils.isBlank(auth.getSecret())
                || auth.getExpirationInSecs() == null
                || passwordReset == null
                || invitation == null) {
            throw new ServiceInitializationException("Not all 'service.token.*' properties are specified.");
        }
    }

    @Data
    public static class Auth {

        private String secret;
        private ExpirationInSecs expirationInSecs;

        @Data
        public static class ExpirationInSecs {

            private int authentication = 3600;
            private int reportingClient = 43200;
            private int refresh = 108000;
            private int access = Integer.MAX_VALUE;

        }

    }

    @Data
    public static class PasswordReset {

        private int retryInSecs = 60;
        private int chars = 100;

    }

    @Data
    public static class Invitation {

        private int chars = 100;

    }

}
