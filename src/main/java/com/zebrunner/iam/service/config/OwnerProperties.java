package com.zebrunner.iam.service.config;

import com.zebrunner.iam.service.exception.ServiceInitializationException;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

@Data
@ConfigurationProperties(prefix = "service.owner")
public class OwnerProperties {

    private String group;

    @PostConstruct
    private void init() {
        if (StringUtils.isBlank(group)) {
            throw new ServiceInitializationException("Information about default owner is not provided.");
        }
    }

}
