package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.Source;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.domain.token.AuthTokenContent;
import com.zebrunner.iam.domain.token.AuthenticationData;
import com.zebrunner.iam.domain.token.RefreshTokenContent;
import com.zebrunner.iam.service.AuthenticationService;
import com.zebrunner.iam.service.JwtService;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.config.TokenProperties;
import com.zebrunner.iam.service.exception.AuthenticationError;
import com.zebrunner.iam.service.exception.ResourceNotFoundError;
import com.zebrunner.iam.service.exception.UnauthorizedAccessError;
import com.zebrunner.iam.web.security.AuthenticatedUser;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final JwtService jwtService;
    private final UserService userService;
    private final TokenProperties tokenProperties;
    private final PermissionService permissionService;
    private final AuthenticationManager authenticationManager;
    private final AuthenticationManager ldapAuthenticationManager;

    @Override
    @Transactional
    public AuthenticationData authenticate(String username, String password) {
        User user = userService.getByUsernameOrEmail(username)
                               .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_USERNAME.withArgs(username));

        Set<String> permissionsSuperset = Set.of();
        try {
            Authentication authenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(), password);

            AuthenticationManager authenticationManager = getAuthenticationManager(user);
            Authentication authentication = authenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            if (authentication.getPrincipal() instanceof AuthenticatedUser) {
                AuthenticatedUser authenticatedUser = (AuthenticatedUser) authentication.getPrincipal();
                permissionsSuperset = authenticatedUser.getPermissionsSuperset();
            }
        } catch (AuthenticationException e) {
            throw AuthenticationError.INVALID_USER_CREDENTIALS.withoutArgs();
        }

        userService.updateLastLogin(user.getId(), Instant.now());

        int expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getAuthentication();
        return generateAuthToken(user, permissionsSuperset, expirationInSecs);
    }

    private AuthenticationManager getAuthenticationManager(User user) {
        return user.getSource() == Source.LDAP
                ? ldapAuthenticationManager
                : authenticationManager;
    }

    @Override
    @Transactional
    public AuthenticationData refresh(String refreshToken) {
        RefreshTokenContent refreshTokenContent = jwtService.parseRefreshToken(refreshToken);
        if (refreshTokenContent.getExpiration().isBefore(Instant.now())) {
            throw AuthenticationError.REFRESH_TOKEN_IS_EXPIRED.withoutArgs();
        }

        Integer userId = refreshTokenContent.getUserId();
        User user = userService.getWithPermissionsById(userId)
                               .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(userId));
        if (UserStatus.INACTIVE.equals(user.getStatus())) {
            throw UnauthorizedAccessError.USER_NOT_ACTIVE.withArgs(userId);
        }
        if (!Objects.equals(user.getPassword(), refreshTokenContent.getPassword())) {
            throw AuthenticationError.USER_PASSWORD_HAS_CHANGED.withoutArgs();
        }

        Set<String> permissionsSuperset = permissionService.getSuperset(user.getGroups(), user.getPermissions());

        // check if the provided token is the access token (access token has greater expiration period then auth one)
        int refreshTokenExpiration = tokenProperties.getAuth().getExpirationInSecs().getRefresh();
        Instant authTokenMaxExpiration = Instant.now().plusSeconds(refreshTokenExpiration);
        boolean isAccessTokenRefresh = authTokenMaxExpiration.isBefore(refreshTokenContent.getExpiration());

        int expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getAuthentication();
        if (isAccessTokenRefresh) {
            userService.updateLastLogin(user.getId(), Instant.now());
            expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getReportingClient();
        }

        return generateAuthToken(user, permissionsSuperset, expirationInSecs);
    }

    @Override
    public String generateServiceAccessToken(Integer userId) {
        User user = userService.getById(userId)
                               .orElseThrow(() -> ResourceNotFoundError.USER_NOT_FOUND_BY_ID.withArgs(userId));
        return jwtService.generateAccessToken(userId, user.getPassword());
    }

    @Override
    public boolean verify(String authToken, Set<String> permissions) {
        AuthTokenContent tokenContent = jwtService.parseAuthToken(authToken);
        return tokenContent.getPermissions() != null
                && tokenContent.getPermissions().containsAll(permissions);
    }

    private AuthenticationData generateAuthToken(User user, Set<String> permissionsSuperset, Integer expirationInSecs) {
        Integer userId = user.getId();
        String username = user.getUsername();

        String authToken = jwtService.generateAuthToken(userId, username, permissionsSuperset, expirationInSecs);
        String refreshToken = jwtService.generateRefreshToken(userId, user.getPassword());

        return AuthenticationData.builder()
                                 .userId(userId)
                                 .userSource(user.getSource())
                                 .permissionsSuperset(permissionsSuperset)
                                 .previousLogin(user.getLastLogin())
                                 .authTokenType("Bearer")
                                 .authToken(authToken)
                                 .authTokenExpirationInSecs(expirationInSecs)
                                 .refreshToken(refreshToken)
                                 .build();
    }

}
