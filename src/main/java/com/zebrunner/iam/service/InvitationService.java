package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.entity.Invitation;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;

import java.util.List;
import java.util.Optional;

public interface InvitationService {

    SearchResult<Invitation> search(SearchCriteria searchCriteria);

    Optional<Invitation> getByToken(String token);

    List<Invitation> create(Integer invitorId, String invitorUsername, List<Invitation> invitations);

    Invitation createOwnerInvitation(Integer invitorId, String ownerEmail);

    void deleteById(Integer id);

}
