package com.zebrunner.iam.service.util;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class StreamUtils {

    private StreamUtils() {
    }

    public static <T, R> Set<R> mapToSet(Collection<T> collection, Function<T, R> mapper) {
        return collection.stream()
                         .map(mapper)
                         .collect(Collectors.toSet());
    }

    public static <T, R> List<R> mapToList(Collection<T> collection, Function<T, R> mapper) {
        return collection.stream()
                         .map(mapper)
                         .collect(Collectors.toList());
    }

    public static <T> List<T> filterToList(Collection<T> collection, Predicate<T> filter) {
        return collection.stream()
                         .filter(filter)
                         .collect(Collectors.toList());
    }

}
