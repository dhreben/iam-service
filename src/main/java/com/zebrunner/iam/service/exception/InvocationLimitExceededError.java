package com.zebrunner.iam.service.exception;

import com.zebrunner.common.eh.exception.InvocationLimitExceededException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum InvocationLimitExceededError implements com.zebrunner.common.eh.error.InvocationLimitExceededError {

    OFTEN_PASSWORD_RESETS(3300);

    private final int code;

    public InvocationLimitExceededException withoutArgs() {
        return new InvocationLimitExceededException(this);
    }

}
