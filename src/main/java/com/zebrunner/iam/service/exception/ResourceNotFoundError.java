package com.zebrunner.iam.service.exception;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ResourceNotFoundError implements com.zebrunner.common.eh.error.ResourceNotFoundError {

    USER_NOT_FOUND_BY_USERNAME(3500),
    USER_NOT_FOUND_BY_ID(3501),
    USER_NOT_FOUND_BY_RESET_TOKEN(3502),
    USER_NOT_FOUND_BY_EMAIL(3503),
    GROUP_NOT_FOUND_BY_ID(3504),
    GROUP_NOT_FOUND_BY_NAME(3505),
    INVITATION_NOT_FOUND_BY_ID(3506),
    INVITATION_NOT_FOUND_BY_TOKEN(3507),
    LDAP_CONFIGURATION_NOT_FOUND(3508),
    PERMISSIONS_NOT_FOUND_BY_NAMES(3509),
    DEFAULT_GROUP_NOT_FOUND(3510);

    private final int code;

    public ResourceNotFoundException withoutArgs() {
        return new ResourceNotFoundException(this);
    }

    public ResourceNotFoundException withArgs(Object... messageArgs) {
        return new ResourceNotFoundException(this, messageArgs);
    }

}
