package com.zebrunner.iam.web.config;

import com.zebrunner.iam.web.filter.CorsFilter;
import com.zebrunner.iam.web.filter.JwtAuthenticationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.ExceptionTranslationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final CorsFilter corsFilter = new CorsFilter();
    private final AccessDeniedHandler accessDeniedHandler;
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final AuthenticationEntryPoint authenticationEntryPoint;

    private static final String[] PUBLIC_API_PATTERNS = new String[]{
            "/v1/auth.*",
            "/v1/invitations\\?token.+",
            "/v1/users/password-resets.*",
            "/v1/users\\?invitation-token.+"
    };

    private static final String[] AUTHENTICATED_API_PATTERNS = new String[]{
            "/v1/groups.*",
            "/v1/invitations.*",
            "/v1/permissions.*",
            "/v1/users.*"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler)
                .and()
                .addFilterAfter(jwtAuthenticationFilter, ExceptionTranslationFilter.class)
                .addFilterBefore(corsFilter, jwtAuthenticationFilter.getClass())
                .authorizeRequests()
                .regexMatchers(PUBLIC_API_PATTERNS).permitAll()
                .regexMatchers(AUTHENTICATED_API_PATTERNS).authenticated();
    }

    @Bean
    public FilterRegistrationBean<JwtAuthenticationFilter> jwtTokenAuthenticationFilterRegistration(JwtAuthenticationFilter filter) {
        FilterRegistrationBean<JwtAuthenticationFilter> registration = new FilterRegistrationBean<>(filter);
        registration.setEnabled(false);
        return registration;
    }

}
