package com.zebrunner.iam.web.documentation.v1;

import com.zebrunner.iam.web.request.v1.ResetPasswordRequest;
import com.zebrunner.iam.web.request.v1.SendPasswordResetRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public interface PasswordResetDocumentedController {

    @Operation(
            summary = "Sends a reset password email.",
            description = "Generates a reset password token and sends it via email.",
            tags = "Password-Reset",
            requestBody = @RequestBody(
                    description = "The email to send the reset password token to.",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = SendPasswordResetRequest.class)
                    ),
                    required = true
            ),
            responses = {
                    @ApiResponse(responseCode = "202", description = "The email sending was successfully submitted."),
                    @ApiResponse(responseCode = "404", description = "Indicates that the account does not exist."),
                    @ApiResponse(responseCode = "429", description = "The reset password email cannot be send so often.")
            }
    )
    void sendResetPasswordEmail(SendPasswordResetRequest sendPasswordResetRequest);

    @Operation(
            summary = "Checks whether a specified reset password token is valid.",
            description = "Checks whether a specified reset password token is valid and password reset is possible.",
            tags = "Password-Reset",
            parameters = @Parameter(name = "reset-token", description = "The reset-token to check for existence.", required = true),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            content = @Content,
                            description = "The reset token is valid, and the reset password operation is possible."
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            content = @Content,
                            description = "Indicates that the password reset token does not exist and operation is not possible."
                    )
            }

    )
    ResponseEntity<Void> checkIfTokenResetIsPossible(String resetToken);

    @Operation(
            summary = "Resets the old password and changes it with a new one.",
            description = "Checks whether a password reset operation is possible, and changes the password.",
            tags = "Password-Reset",
            requestBody = @RequestBody(
                    description = "Reset password token and a new password.",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = ResetPasswordRequest.class)
                    )
            ),
            responses = {
                    @ApiResponse(responseCode = "204", description = "The password was updated successfully."),
                    @ApiResponse(responseCode = "400", description = "Indicates that the password reset operation is not possible.")
            }
    )
    void resetPassword(ResetPasswordRequest resetPasswordRequest);
}
