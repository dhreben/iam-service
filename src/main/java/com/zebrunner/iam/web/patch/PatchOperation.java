package com.zebrunner.iam.web.patch;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PatchOperation {

    @JsonProperty("replace")
    REPLACE

}
