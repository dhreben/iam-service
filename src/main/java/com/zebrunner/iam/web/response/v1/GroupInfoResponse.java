package com.zebrunner.iam.web.response.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.iam.domain.entity.Permission;
import lombok.Data;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@JGlobalMap
public class GroupInfoResponse {

    private Integer id;
    private String name;
    @JsonProperty("isDefault")
    private Boolean isDefault;
    private Boolean invitable;
    private List<String> permissions;

    @JMapConversion(from = {"permissions"}, to = {"permissions"})
    public List<String> convertPermissions(Set<Permission> permissions) {
        return permissions.stream()
                          .map(Permission::getName)
                          .collect(Collectors.toList());
    }

}
