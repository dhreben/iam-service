package com.zebrunner.iam.web.mapper;

import com.googlecode.jmapper.JMapper;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MapperImpl implements Mapper {

    private final Map<String, JMapper> identifierToMapper = new ConcurrentHashMap<>();

    @Override
    public <D, T> D map(T t, Class<D> clazz) {
        String mapperIdentifier = clazz.getCanonicalName() + ":" + t.getClass().getCanonicalName();
        JMapper<D, T> mapper = identifierToMapper
                .computeIfAbsent(mapperIdentifier, s -> new JMapper<>(clazz, t.getClass()));
        return mapper.getDestination(t);
    }
}
