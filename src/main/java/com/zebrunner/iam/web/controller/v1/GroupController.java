package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.SortOrder;
import com.zebrunner.iam.service.GroupService;
import com.zebrunner.iam.service.exception.MalformedInputError;
import com.zebrunner.iam.service.exception.ResourceNotFoundError;
import com.zebrunner.iam.web.documentation.v1.GroupDocumentedController;
import com.zebrunner.iam.web.mapper.Mapper;
import com.zebrunner.iam.web.patch.PatchOperation;
import com.zebrunner.iam.web.patch.PatchRequest;
import com.zebrunner.iam.web.patch.ValidPatch;
import com.zebrunner.iam.web.request.v1.SaveGroupRequest;
import com.zebrunner.iam.web.response.v1.FullGroupInfoResponse;
import com.zebrunner.iam.web.response.v1.GroupInfoResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.DataBinder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Positive;

@Validated
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/groups", produces = MediaType.APPLICATION_JSON_VALUE)
public class GroupController implements GroupDocumentedController {

    private final Mapper mapper;
    private final GroupService groupService;

    @InitBinder
    private void activateDirectFieldAccess(DataBinder dataBinder) {
        dataBinder.initDirectFieldAccess();
    }

    @Override
    @GetMapping
    @PreAuthorize("hasPermission('iam:groups:read') or #isPublic")
    public SearchResult<FullGroupInfoResponse> search(@RequestParam(name = "query", required = false) String query,
                                                      @RequestParam(name = "sortBy", required = false) String sortBy,
                                                      @RequestParam(name = "sortOrder", required = false) SortOrder sortOrder,
                                                      @RequestParam(name = "page", defaultValue = "1") Integer page,
                                                      @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize,
                                                      @RequestParam(name = "public", defaultValue = "true") boolean isPublic) {
        SearchCriteria searchCriteria = new SearchCriteria(query, sortBy, sortOrder, page, pageSize, isPublic);
        SearchResult<Group> searchResult = groupService.search(searchCriteria);
        return new SearchResult<>(searchResult, mapper.map(searchResult.getResults(), FullGroupInfoResponse.class));
    }

// this endpoint needs to be reviewed
//    @Override
//    @GetMapping("/default")
//    @PreAuthorize("hasPermission('iam:groups:read')")
//    public GroupDto getDefault() {
//        Group group = groupService.getDefault()
//                                  .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.DEFAULT_GROUP_NOT_FOUND));
//        return mapper.map(group, GroupDto.class);
//    }

    @Override
    @GetMapping("/{id}")
    @PreAuthorize("hasPermission('iam:groups:read')")
    public GroupInfoResponse getById(@PathVariable("id") @Positive Integer id) {
        Group group = groupService.getById(id)
                                  .orElseThrow(() -> ResourceNotFoundError.GROUP_NOT_FOUND_BY_ID.withArgs(id));
        return mapper.map(group, GroupInfoResponse.class);
    }

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasPermission('iam:groups:update')")
    public GroupInfoResponse create(@RequestBody SaveGroupRequest saveGroupRequest) {
        Group group = mapper.map(saveGroupRequest, Group.class);
        group = groupService.save(group);
        return mapper.map(group, GroupInfoResponse.class);
    }

    @Override
    @PutMapping("/{id}")
    @PreAuthorize("hasPermission('iam:groups:update')")
    public GroupInfoResponse update(@PathVariable("id") @Positive Integer id,
                                    @RequestBody SaveGroupRequest saveGroupRequest) {
        Group group = mapper.map(saveGroupRequest, Group.class);
        group.setId(id);
        group = groupService.save(group);
        return mapper.map(group, GroupInfoResponse.class);
    }

    @Override
    @PatchMapping("/{id}")
    @PreAuthorize("hasPermission('iam:groups:update')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void setDefault(@PathVariable("id") @Positive Integer id,
                           @ValidPatch(op = PatchOperation.REPLACE, path = "/isDefault")
                           @RequestBody @Valid PatchRequest patchRequest) {
        Boolean isDefault = patchRequest.getReplaceValue("/isDefault", Boolean.class);
        if (isDefault != null) {
            groupService.setDefault(id, isDefault);
        } else {
            throw MalformedInputError.REQUIRED_VALUE_NOT_PROVIDED.withoutArgs();
        }
    }

    @Override
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasPermission('iam:groups:delete')")
    public void delete(@PathVariable("id") @Positive Integer id) {
        groupService.deleteById(id);
    }

}
