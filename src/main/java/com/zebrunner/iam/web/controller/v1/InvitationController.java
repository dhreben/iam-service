package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.iam.domain.entity.Invitation;
import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.SortOrder;
import com.zebrunner.iam.service.InvitationService;
import com.zebrunner.iam.service.exception.ResourceNotFoundError;
import com.zebrunner.iam.web.documentation.v1.InvitationDocumentedController;
import com.zebrunner.iam.web.mapper.Mapper;
import com.zebrunner.iam.web.request.v1.SaveInvitationRequest;
import com.zebrunner.iam.web.response.v1.InvitationInfoResponse;
import com.zebrunner.iam.web.security.AuthenticatedUser;
import com.zebrunner.iam.web.security.Principal;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.stream.Collectors;

@Validated
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/invitations", produces = MediaType.APPLICATION_JSON_VALUE)
public class InvitationController implements InvitationDocumentedController {

    private final Mapper mapper;
    private final InvitationService invitationService;

    @Override
    @GetMapping
    @PreAuthorize("hasAnyPermission('iam:invitations:read', 'iam:invitations:delete')")
    public SearchResult<InvitationInfoResponse> search(@RequestParam(name = "query", required = false) String query,
                                                       @RequestParam(name = "sortBy", required = false) String sortBy,
                                                       @RequestParam(name = "sortOrder", required = false) SortOrder sortOrder,
                                                       @RequestParam(name = "page", defaultValue = "1") Integer page,
                                                       @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize) {
        SearchCriteria searchCriteria = new SearchCriteria(query, sortBy, sortOrder, page, pageSize);
        SearchResult<Invitation> searchResult = invitationService.search(searchCriteria);
        return new SearchResult<>(searchResult, mapper.map(searchResult.getResults(), InvitationInfoResponse.class));
    }

    @Override
    @GetMapping(params = "token")
    public InvitationInfoResponse getInvitation(@RequestParam("token") @NotEmpty String token) {
        return invitationService.getByToken(token)
                                .map(invitation -> mapper.map(invitation, InvitationInfoResponse.class))
                                .orElseThrow(ResourceNotFoundError.INVITATION_NOT_FOUND_BY_TOKEN::withoutArgs);
    }

    @Override
    @PostMapping
    @PreAuthorize("hasPermission('iam:invitations:update')")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<InvitationInfoResponse> inviteUsers(@Principal AuthenticatedUser authenticatedUser,
                                                   @RequestBody @Valid List<SaveInvitationRequest> saveInvitationRequests) {
        List<Invitation> invitations = saveInvitationRequests.stream()
                                                             .map(this::toInvitation)
                                                             .collect(Collectors.toList());
        return invitationService.create(authenticatedUser.getUserId(), authenticatedUser.getUsername(), invitations)
                                .stream()
                                .map(invitation -> mapper.map(invitation, InvitationInfoResponse.class))
                                .collect(Collectors.toList());
    }

    private Invitation toInvitation(SaveInvitationRequest saveInvitationRequest) {
        return mapper.map(saveInvitationRequest, Invitation.class);
    }

    @Override
    @DeleteMapping("/{id}")
    @PreAuthorize("hasPermission('iam:invitations:delete')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteInvitation(@PathVariable("id") @Positive Integer id) {
        invitationService.deleteById(id);
    }

}
