package com.zebrunner.iam.persistance.impl;

import io.jsonwebtoken.lang.Assert;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.convert.QueryByExamplePredicateBuilder;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class ExampleSpecification<T> implements Specification<T> {

    private final Example<T> example;

    public ExampleSpecification(Example<T> example) {
        Assert.notNull(example, "Example must not be null!");
        this.example = example;
    }

    @Override
    public Predicate toPredicate(@NonNull Root<T> root,
                                 @NonNull CriteriaQuery<?> query,
                                 @NonNull CriteriaBuilder criteriaBuilder) {
        return QueryByExamplePredicateBuilder.getPredicate(root, criteriaBuilder, example);
    }

}
