package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.Group;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer> {

    @EntityGraph("group-with-permissions")
    Optional<Group> findWithPermissionsById(Integer id);

    Optional<Group> findByName(String name);

    @EntityGraph("group-with-permissions")
    Optional<Group> findByIsDefaultTrue();

    @Query("select count(u.id) from Group g left join g.users u where g.id = :id")
    Integer countUsersById(@Param("id") Integer id);

    Optional<Group> findByInvitableTrueAndId(Integer id);

    @Modifying
    @Query("update Group g set g.isDefault = false")
    void setAllNotDefault();

}
